/*!
 * Jodit Editor PRO (https://xdsoft.net/jodit/)
 * See LICENSE.md in the project root for license information.
 * Copyright (c) 2013-2021 Valeriy Chupurnov. All rights reserved. https://xdsoft.net/jodit/pro/
 */

Jodit.defaultOptions.license = '5573M-20L62-BTOKJ-S7P5Z';
Jodit.defaultOptions.language = 'en';

Jodit.defaultOptions.extraPlugins = [
	//'autocomplete',
	// 'backup',
	// 'change-case',
	//'color-picker',
	//'emoji',
	// 'finder',
	//'google-search',
	//'paste-code',
	//'keyboard',
	// 'show-blocks',
	// 'button-generator',
	// 'tune-block'
];

Jodit.defaultOptions.disablePlugins = [];
