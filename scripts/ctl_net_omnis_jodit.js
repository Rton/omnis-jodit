
/**
 * Copyright (c) 2020-2021 Antonio Ratsimbazafy (https://gitlab.com/Rton). Permission is hereby granted, free of charge, to any
 * person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
 * AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

 ctrl_net_omnis_jodit.prototype = (function() {

    /** Omnis Studio Javascipt component, built using the JSON defined control editor.
     * Omnis Studio Javascript controller with Jodit a richtext editor
     * 
     *  Update option uses OmnisUpdateMarkers as follows:
     *  - replaces all code between _Begin and _End
     *  - looks for occurrences between _Start and _next, adding at _next if does not exist
     *  - inserts functions for new custom methods at _CustomMethod_New
     *    (parameters for existing custom method functions also updated if changed)
     */

   /****** CONSTANTS ******/
   var PROPERTIES = {
    // <OmnisUpdateMarker_PropertyConstants_Begin>
    autoscroll: "$autoscroll",
    enablekeypressevent: "$enablekeypressevent",
    callbackdelay: "$callbackdelay"
    // <OmnisUpdateMarker_PropertyConstants_End>
    };

    var EVENTS = {
        // <OmnisUpdateMarker_EventConstants_Begin>
        evTyping: 1
        // <OmnisUpdateMarker_EventConstants_End>
    };

   

    /** The control prototype - inherited from base class prototype */
    var ctrl = new ctrl_base();
    
    var eventJodit = null;
    var afterTimeout;
    /**
     * class initialization, must be called by constructor
     * this function should initialize class variables
     * IMPORTANT:
     * initialization is separated out from the constructor
     * as the base class constructor is not called when a
     * class is subclassed.
     * all subclass constructors must call their own
     * init_class_inst function which in turn must call the
     * superclass.init_class_inst function
     */
     ctrl.init_class_inst = function()
     {
         // install superclass prototype so we can than call superclass methods
         // using this.superclass.method_name.call(this[,...])
         this.superclass = ctrl_base.prototype;
         // call our superclass class initializer
         this.superclass.init_class_inst.call( this );
         // initialize class specific stuff
         this.mData = "";
         this.mDefaultText = "EMPTY";
         // initialize jodit stuff
         this.editor = null;     // jodit editor himself
         this.nodeJodit = null;  // jodit div container 
         this.joditNodeChildren = null; // all children of the container (toolbar + editable)
         this.keycode = -1;
         // <OmnisUpdateMarker_InitVars_Start>
  
         this.mautoscroll = false;
         this.msource = false;
         this.mbold = false;
         this.mstrikethrough = false;
         this.munderline = false;
         this.mitalic = false;
         this.mul = false;
         this.mol = false;
         this.moutdent = false;
         this.mindent = false;
         this.mfont = false;
         this.mfontsize = false;
         this.mbrush = false;
         this.mparagraph = false;
         this.mimage = false;
         this.mvideo = false;
         this.mtable = false;
         this.mlink = false;
         this.malign = false;
         this.mundo = false;
         this.mredo = false;
         this.mhr = false;
         this.mcopyformat = false;
         this.msymbol = false;
         this.mfullsize = false;
         this.mprint = false;
         this.menablekeypressevent = false;
         this.mcallbackdelay = 0;
         // <OmnisUpdateMarker_InitVars_Next>
     };
 

    /**
     * On destruct method for Omnis Control
     */
    ctrl.delete_class_inst = function()
    {
        this.removeInstance();
        this.superclass.delete_class_inst.call(this); // Call superclass version to perform standard deletion procedure.
    };

    /**
     * Remove all jodit instances inside the control
     */
    ctrl.removeInstance = function(){
        if (Jodit.instances) {
            Object.keys(Jodit.instances).forEach(function (id) {
                Jodit.instances[id].destruct();
            });
        }
    }

    /**
     * Display jodit inside a div
     * Handle document with DOM property 
     */

    ctrl.createJoditDiv =  function (){
        var joditDiv = document.createElement('div');  		// create a div for our object
        var html = '<div id="main_container" class="container-app" style="cursor: text;clear: both;">'+
                '<div class="result" style="line-height: 1.6;">'+
                    '<textarea class="area-editor"></textarea>'+
                '</div>'+
        '</div>';
        joditDiv.innerHTML = html;
        joditDiv.style.width = joditDiv.style.height = "100%";
        this.clientElem.appendChild(joditDiv);
        var client_elem = this.getClientElem();
        this.replacedId = client_elem.firstChild.id;
        client_elem.style.zIndex = 0;
    }

    /**
     * Instantiation for jodit
     * @param eventFunc Event to be registered and triggered
     */
     ctrl.joditNewInstance = function(eventFunc){
        this.createJoditDiv();
        this.editor = new Jodit('.area-editor', {
            hotkeys: {
                redo: 'ctrl+z',
                undo: 'ctrl+y,ctrl+shift+z',
                indent: 'ctrl+]',
                outdent: 'ctrl+[',
                bold: 'ctrl+b',
                italic: 'ctrl+i',
                removeFormat: 'ctrl+shift+m',
                insertOrderedList: 'ctrl+shift+7',
                insertUnorderedList: 'ctrl+shift+8',
                openSearchDialog: 'ctrl+f',
                openReplaceDialog: 'ctrl+r',
            },
            events: {
                afterPaste: function(event){
                    omnisCtrl.update();
                },
                onKeyPress : function(event){
                    omnisCtrl.update();
                }
            },
            autofocus: false,
            fullsize: true,
            globalFullSize: false,
            zIndex: 0,
            readonly: false,
            activeButtonsInReadOnly: ['source', 'fullsize', 'print', 'dots'], //'about'
            toolbarButtonSize: 'small',
            textIcons: false,
            theme: 'default',
            saveModeInCookie: false,
            spellcheck: true,
            editorCssClass: false,
            triggerChangeEvent: true,
            width: 'auto',
            height: 'auto',
            minHeight: 100,
            direction: '',
            language: 'fr',
            debugLanguage: false,
            i18n: 'fr',
            tabIndex: -1,
            toolbar: true,
            enter: "BR",
            useSplitMode: false,
            colors: {
                greyscale:  ['#000000', '#434343', '#666666', '#999999', '#B7B7B7', '#CCCCCC', '#D9D9D9', '#EFEFEF', '#F3F3F3', '#FFFFFF'],
                palette:    ['#980000', '#FF0000', '#FF9900', '#FFFF00', '#00F0F0', '#00FFFF', '#4A86E8', '#0000FF', '#9900FF', '#FF00FF'],
                full: [
                    '#E6B8AF', '#F4CCCC', '#FCE5CD', '#FFF2CC', '#D9EAD3', '#D0E0E3', '#C9DAF8', '#CFE2F3', '#D9D2E9', '#EAD1DC',
                    '#DD7E6B', '#EA9999', '#F9CB9C', '#FFE599', '#B6D7A8', '#A2C4C9', '#A4C2F4', '#9FC5E8', '#B4A7D6', '#D5A6BD',
                    '#CC4125', '#E06666', '#F6B26B', '#FFD966', '#93C47D', '#76A5AF', '#6D9EEB', '#6FA8DC', '#8E7CC3', '#C27BA0',
                    '#A61C00', '#CC0000', '#E69138', '#F1C232', '#6AA84F', '#45818E', '#3C78D8', '#3D85C6', '#674EA7', '#A64D79',
                    '#85200C', '#990000', '#B45F06', '#BF9000', '#38761D', '#134F5C', '#1155CC', '#0B5394', '#351C75', '#733554',
                    '#5B0F00', '#660000', '#783F04', '#7F6000', '#274E13', '#0C343D', '#1C4587', '#073763', '#20124D', '#4C1130'
                ]
            },
            "uploader": {
                "insertImageAsBase64URI": true
            },
            colorPickerDefaultTab: 'background',
            imageDefaultWidth: 300,
            removeButtons: [''],
            disablePlugins: [],
            extraButtons: [],
            sizeLG: 900,
            sizeMD: 700,
            sizeSM: 400,
            buttons: [
                'source', '|',
                'bold',
                'strikethrough',
                'underline',
                'italic', '|',
                'ul',
                'ol', '|',
                'outdent', 'indent',  '|',
                'font',
                'fontsize',
                'brush',
                'paragraph', '|',
                'image',
                'video',
                'table',
                'link', '|',
                'align', 'undo', 'redo', '|',
                'hr',
                'eraser',
                'copyformat', '|',
                'symbol',
                'fullsize',
                'print'
                // 'about'
            ],
            buttonsXS: [
                'source', '|',
                'bold',
                'strikethrough',
                'underline',
                'italic', '|',
                'ul',
                'ol', '|',
                'outdent', 'indent',  '|',
                'font',
                'fontsize',
                'brush',
                'paragraph', '|',
                'image',
                'video',
                'table',
                'link', '|',
                'align', 'undo', 'redo', '|',
                'hr',
                'eraser',
                'copyformat', '|',
                'symbol',
                'fullsize',
                'print'
                
            ]
        });
        this.editor.onblur = eventFunc;
        this.editor.onkeyup =this.handleKey;
        this.editor.onpaste = eventFunc;
        this.editor.oncut = eventFunc;
        this.setProperty(PROPERTIES.autoscroll, this.mautoscroll);
        var client_elem = this.getClientElem();
        this.nodeJodit = client_elem.children[0].children[0].children[0].children[0];
        this.nodeJodit.style.overflow = this.mautoscroll ? "auto" : "hidden";
        var dataname = this.getData();

        if (this.mData != dataname) { // only execute the following code if the value of the $dataname variable has changed.
            this.mData = dataname;
            if (dataname) {
                this.nodeJodit.innerHTML = this.mData;
            }
            else {
                this.nodeJodit.innerHTML = this.mDefaultText;
            }
        }
        var omnisCtrl = this;

		$('ul').css('line-height',2);

        $(".area-editor").on("change focus input paste keyup", function(event){    
            eventJodit = true;
            omnisCtrl.update();
            eventJodit = null;
            
            omnisCtrl.handleEvent({"type": "typing"})
        });
      
    }


    /**
     * Initializes the control instance from element attributes.
     * Must be called after control is constructed by the constructor.
     * @param form      Reference to the parent form.
     * @param elem      The html element the control belongs to.
     * @param rowCtrl   Pointer to a complex grid control if this control belongs to a cgrid.
     * @param rowNumber The row number this control belongs to if it belongs to a cgrid.
     * @returns {boolean}   True if the control is a container.
     */
    ctrl.init_ctrl_inst = function( form, elem, rowCtrl, rowNumber )
    {
        // call our superclass init_ctrl_inst
        this.superclass.init_ctrl_inst.call( this, form, elem, rowCtrl, rowNumber );

        //Control-specific initialization:
        // assign the methods for our events
        var eventFunc = this.mEventFunction;
        var client_elem = this.getClientElem();
        var dataprops = client_elem.getAttribute('data-props');
        var datapropsobj = JSON.parse(dataprops);

        this.mautoscroll = datapropsobj.autoscroll;
        // <OmnisUpdateMarker_CallSetProperty_Start>
        
        var attValue = datapropsobj.autoscroll;
        this.setProperty(PROPERTIES.autoscroll, attValue);
        
        var attValue = datapropsobj.enablekeypressevent;
        this.setProperty(PROPERTIES.enablekeypressevent, attValue);
        
        var attValue = datapropsobj.callbackdelay;
        this.setProperty(PROPERTIES.callbackdelay, attValue);
        
        // <OmnisUpdateMarker_CallSetProperty_Next>

        // Get the key value pairs of the field PROPERTIES
        const entry = Object.entries(PROPERTIES);
        
        let mapping = [...entry];
        for (let i = 0; i < mapping.length; i++) {
            const element = mapping[i];
            // cast to object then get correspondancy properties
            const obj = Object.assign(element[0]);
            mapping[i][1] = this.getProperty(PROPERTIES[obj]);
        }
        this.removeInstance();

        if(this.getTrueVisibility(null)){
            this.joditNewInstance(eventFunc);
        }
        
        // return true if our control is a container and the
        // children require installing via this.form.InstallChildren
        return false
    };
    /**
     * The control's data has changed. The contents may need to be updated.
     *
     * @param {String} what    Specifies which part of the data has changed:
     *                 ""              - The entire data has changed
     *                 "#COL"          - A single column in the $dataname list (specified by 'row' and 'col') or a row's column (specified by 'col')
     *                 "#LSEL"         - The selected line of the $dataname list has changed (specified by 'row')
     *                 "#L"            - The current line of the $dataname list has changed  (specified by 'row')
     *                 "#LSEL_ALL"     - All lines in the $dataname list have been selected.
     *                 "#NCELL"        - An individual cell in a (nested) list. In this case, 'row' is an array of row & column numbers.
     *                                  of the form "row1,col1,row2,col2,..."
     *
     * @param {Number} row             If specified, the row number in a list (range = 1..n) to which the change pertains.
     *                                 If 'what' is "#NCELL", this must be an array of row and col numbers. Optionally, a modifier may be
     *                                 added as the final array element, to change which part of the nested data is to be changed. (Currently only "#L" is supported)
     *
     * @param {Number|String} col      If specified, the column in a list row (range = 1..n or name) to which the change pertains.
     */
    ctrl.updateCtrl = function(what, row, col, mustUpdate) // asynchronous method
    {
        if (!this.getTrueVisibility()) // Only update when control is visible
            return;
        if(!this.editor){
            this.joditNewInstance();
        }
        var elem = this.getClientElem();
        //Access div that contains jodit wich is a nodelist
        let nodeList = elem.children[0].children[0].children[0].childNodes;

        //Accessing the nodelist 
        /** The HTML code is like below  */
        /*  # First children
            <div class="jodit-toolbar__box"><div class="jodit-toolbar-editor-collection jodit-toolbar-editor-collection_mode_horizontal jodit-toolbar-editor-collection_size_small" style="width: auto;"><input tab-index="-1" style="width: 0; height:0; position: absolute; visibility: hidden;"><div class="jodit-ui-group jodit-ui-group_size_middle"><div class="jodit-ui-group jodit-ui-group_size_middle"><span class="jodit-toolbar-button jodit-toolbar-button_bold jodit-ui-group__bold jodit-toolbar-button_size_small" role="listitem" data-ref="bold" ref="bold" aria-label="Gras"><button class="jodit-toolbar-button__button" type="button" role="button" aria-pressed="false" tabindex="-1"><span class="jodit-toolbar-button__icon"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792" class="jodit-icon_bold jodit-icon"> <path d="M747 1521q74 32 140 32 376 0 376-335 0-114-41-180-27-44-61.5-74t-67.5-46.5-80.5-25-84-10.5-94.5-2q-73 0-101 10 0 53-.5 159t-.5 158q0 8-1 67.5t-.5 96.5 4.5 83.5 12 66.5zm-14-746q42 7 109 7 82 0 143-13t110-44.5 74.5-89.5 25.5-142q0-70-29-122.5t-79-82-108-43.5-124-14q-50 0-130 13 0 50 4 151t4 152q0 27-.5 80t-.5 79q0 46 1 69zm-541 889l2-94q15-4 85-16t106-27q7-12 12.5-27t8.5-33.5 5.5-32.5 3-37.5.5-34v-65.5q0-982-22-1025-4-8-22-14.5t-44.5-11-49.5-7-48.5-4.5-30.5-3l-4-83q98-2 340-11.5t373-9.5q23 0 68.5.5t67.5.5q70 0 136.5 13t128.5 42 108 71 74 104.5 28 137.5q0 52-16.5 95.5t-39 72-64.5 57.5-73 45-84 40q154 35 256.5 134t102.5 248q0 100-35 179.5t-93.5 130.5-138 85.5-163.5 48.5-176 14q-44 0-132-3t-132-3q-106 0-307 11t-231 12z"></path> </svg></span><span class="jodit-toolbar-button__text"></span></button></span><span class="jodit-toolbar-button jodit-toolbar-button_image jodit-ui-group__image jodit-toolbar-button_size_small" role="listitem" data-ref="image" ref="image" aria-label="Insérer une image"><button class="jodit-toolbar-button__button" type="button" role="button" aria-pressed="false" tabindex="-1"><span class="jodit-toolbar-button__icon"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792" class="jodit-icon_image jodit-icon"> <path d="M576 576q0 80-56 136t-136 56-136-56-56-136 56-136 136-56 136 56 56 136zm1024 384v448h-1408v-192l320-320 160 160 512-512zm96-704h-1600q-13 0-22.5 9.5t-9.5 22.5v1216q0 13 9.5 22.5t22.5 9.5h1600q13 0 22.5-9.5t9.5-22.5v-1216q0-13-9.5-22.5t-22.5-9.5zm160 32v1216q0 66-47 113t-113 47h-1600q-66 0-113-47t-47-113v-1216q0-66 47-113t113-47h1600q66 0 113 47t47 113z"></path> </svg></span><span class="jodit-toolbar-button__text"></span></button></span><span class="jodit-toolbar-button jodit-toolbar-button_video jodit-ui-group__video jodit-toolbar-button_size_small" role="listitem" data-ref="video" ref="video" aria-label="Insérer une vidéo"><button class="jodit-toolbar-button__button" type="button" role="button" aria-pressed="false" tabindex="-1"><span class="jodit-toolbar-button__icon"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792" class="jodit-icon_video jodit-icon"> <path d="M1792 352v1088q0 42-39 59-13 5-25 5-27 0-45-19l-403-403v166q0 119-84.5 203.5t-203.5 84.5h-704q-119 0-203.5-84.5t-84.5-203.5v-704q0-119 84.5-203.5t203.5-84.5h704q119 0 203.5 84.5t84.5 203.5v165l403-402q18-19 45-19 12 0 25 5 39 17 39 59z"></path> </svg></span><span class="jodit-toolbar-button__text"></span></button></span><span class="jodit-toolbar-button jodit-toolbar-button_table jodit-ui-group__table jodit-toolbar-button_size_small" role="listitem" data-ref="table" ref="table" aria-label="Insérer un tableau"><button class="jodit-toolbar-button__button" type="button" role="button" aria-pressed="false" tabindex="-1"><span class="jodit-toolbar-button__icon"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792" class="jodit-icon_table jodit-icon"> <path d="M576 1376v-192q0-14-9-23t-23-9h-320q-14 0-23 9t-9 23v192q0 14 9 23t23 9h320q14 0 23-9t9-23zm0-384v-192q0-14-9-23t-23-9h-320q-14 0-23 9t-9 23v192q0 14 9 23t23 9h320q14 0 23-9t9-23zm512 384v-192q0-14-9-23t-23-9h-320q-14 0-23 9t-9 23v192q0 14 9 23t23 9h320q14 0 23-9t9-23zm-512-768v-192q0-14-9-23t-23-9h-320q-14 0-23 9t-9 23v192q0 14 9 23t23 9h320q14 0 23-9t9-23zm512 384v-192q0-14-9-23t-23-9h-320q-14 0-23 9t-9 23v192q0 14 9 23t23 9h320q14 0 23-9t9-23zm512 384v-192q0-14-9-23t-23-9h-320q-14 0-23 9t-9 23v192q0 14 9 23t23 9h320q14 0 23-9t9-23zm-512-768v-192q0-14-9-23t-23-9h-320q-14 0-23 9t-9 23v192q0 14 9 23t23 9h320q14 0 23-9t9-23zm512 384v-192q0-14-9-23t-23-9h-320q-14 0-23 9t-9 23v192q0 14 9 23t23 9h320q14 0 23-9t9-23zm0-384v-192q0-14-9-23t-23-9h-320q-14 0-23 9t-9 23v192q0 14 9 23t23 9h320q14 0 23-9t9-23zm128-320v1088q0 66-47 113t-113 47h-1344q-66 0-113-47t-47-113v-1088q0-66 47-113t113-47h1344q66 0 113 47t47 113z"></path> </svg></span><span class="jodit-toolbar-button__text"></span></button></span><span class="jodit-toolbar-button jodit-toolbar-button_link jodit-ui-group__link jodit-toolbar-button_size_small" role="listitem" data-ref="link" ref="link" aria-label="Insérer un lien"><button class="jodit-toolbar-button__button" type="button" role="button" aria-pressed="false" tabindex="-1"><span class="jodit-toolbar-button__icon"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792" class="jodit-icon_link jodit-icon"> <path d="M1520 1216q0-40-28-68l-208-208q-28-28-68-28-42 0-72 32 3 3 19 18.5t21.5 21.5 15 19 13 25.5 3.5 27.5q0 40-28 68t-68 28q-15 0-27.5-3.5t-25.5-13-19-15-21.5-21.5-18.5-19q-33 31-33 73 0 40 28 68l206 207q27 27 68 27 40 0 68-26l147-146q28-28 28-67zm-703-705q0-40-28-68l-206-207q-28-28-68-28-39 0-68 27l-147 146q-28 28-28 67 0 40 28 68l208 208q27 27 68 27 42 0 72-31-3-3-19-18.5t-21.5-21.5-15-19-13-25.5-3.5-27.5q0-40 28-68t68-28q15 0 27.5 3.5t25.5 13 19 15 21.5 21.5 18.5 19q33-31 33-73zm895 705q0 120-85 203l-147 146q-83 83-203 83-121 0-204-85l-206-207q-83-83-83-203 0-123 88-209l-88-88q-86 88-208 88-120 0-204-84l-208-208q-84-84-84-204t85-203l147-146q83-83 203-83 121 0 204 85l206 207q83 83 83 203 0 123-88 209l88 88q86-88 208-88 120 0 204 84l208 208q84 84 84 204z"></path> </svg></span><span class="jodit-toolbar-button__text"></span></button></span><div class="jodit-ui-separator"></div><span class="jodit-toolbar-button jodit-toolbar-button_left jodit-toolbar-button_with-trigger_true jodit-ui-group__left jodit-toolbar-button_size_small" role="listitem" data-ref="left" ref="left" aria-label="Alignement"><button class="jodit-toolbar-button__button" type="button" role="button" aria-pressed="false" tabindex="-1"><span class="jodit-toolbar-button__icon"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792" class="jodit-icon_left jodit-icon"> <path d="M1792 1344v128q0 26-19 45t-45 19h-1664q-26 0-45-19t-19-45v-128q0-26 19-45t45-19h1664q26 0 45 19t19 45zm-384-384v128q0 26-19 45t-45 19h-1280q-26 0-45-19t-19-45v-128q0-26 19-45t45-19h1280q26 0 45 19t19 45zm256-384v128q0 26-19 45t-45 19h-1536q-26 0-45-19t-19-45v-128q0-26 19-45t45-19h1536q26 0 45 19t19 45zm-384-384v128q0 26-19 45t-45 19h-1152q-26 0-45-19t-19-45v-128q0-26 19-45t45-19h1152q26 0 45 19t19 45z"></path> </svg></span><span class="jodit-toolbar-button__text"></span></button><span role="trigger" class="jodit-toolbar-button__trigger"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 10 10"> <path d="M.941 4.523a.75.75 0 1 1 1.06-1.06l3.006 3.005 3.005-3.005a.75.75 0 1 1 1.06 1.06l-3.549 3.55a.75.75 0 0 1-1.168-.136L.941 4.523z"></path> </svg></span></span><span class="jodit-toolbar-button jodit-toolbar-button_undo jodit-ui-group__undo jodit-toolbar-button_size_small" role="listitem" data-ref="undo" ref="undo" aria-label="Défaire" disabled="disabled"><button class="jodit-toolbar-button__button" type="button" role="button" aria-pressed="false" tabindex="-1" disabled="disabled"><span class="jodit-toolbar-button__icon"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792" class="jodit-icon_undo jodit-icon"> <path d="M1664 896q0 156-61 298t-164 245-245 164-298 61q-172 0-327-72.5t-264-204.5q-7-10-6.5-22.5t8.5-20.5l137-138q10-9 25-9 16 2 23 12 73 95 179 147t225 52q104 0 198.5-40.5t163.5-109.5 109.5-163.5 40.5-198.5-40.5-198.5-109.5-163.5-163.5-109.5-198.5-40.5q-98 0-188 35.5t-160 101.5l137 138q31 30 14 69-17 40-59 40h-448q-26 0-45-19t-19-45v-448q0-42 40-59 39-17 69 14l130 129q107-101 244.5-156.5t284.5-55.5q156 0 298 61t245 164 164 245 61 298z"></path> </svg></span><span class="jodit-toolbar-button__text"></span></button></span><span class="jodit-toolbar-button jodit-toolbar-button_redo jodit-ui-group__redo jodit-toolbar-button_size_small" role="listitem" data-ref="redo" ref="redo" aria-label="Refaire" disabled="disabled"><button class="jodit-toolbar-button__button" type="button" role="button" aria-pressed="false" tabindex="-1" disabled="disabled"><span class="jodit-toolbar-button__icon"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792" class="jodit-icon_redo jodit-icon"> <path d="M1664 256v448q0 26-19 45t-45 19h-448q-42 0-59-40-17-39 14-69l138-138q-148-137-349-137-104 0-198.5 40.5t-163.5 109.5-109.5 163.5-40.5 198.5 40.5 198.5 109.5 163.5 163.5 109.5 198.5 40.5q119 0 225-52t179-147q7-10 23-12 14 0 25 9l137 138q9 8 9.5 20.5t-7.5 22.5q-109 132-264 204.5t-327 72.5q-156 0-298-61t-245-164-164-245-61-298 61-298 164-245 245-164 298-61q147 0 284.5 55.5t244.5 156.5l130-129q29-31 70-14 39 17 39 59z"></path> </svg></span><span class="jodit-toolbar-button__text"></span></button></span><div class="jodit-ui-separator"></div><span class="jodit-toolbar-button jodit-toolbar-button_hr jodit-ui-group__hr jodit-toolbar-button_size_small" role="listitem" data-ref="hr" ref="hr" aria-label="Insérer une ligne horizontale"><button class="jodit-toolbar-button__button" type="button" role="button" aria-pressed="false" tabindex="-1"><span class="jodit-toolbar-button__icon"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792" class="jodit-icon_hr jodit-icon"> <path d="M1600 736v192q0 40-28 68t-68 28h-1216q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h1216q40 0 68 28t28 68z"></path> </svg></span><span class="jodit-toolbar-button__text"></span></button></span><span class="jodit-toolbar-button jodit-toolbar-button_eraser jodit-ui-group__eraser jodit-toolbar-button_size_small" role="listitem" data-ref="eraser" ref="eraser" aria-label="Supprimer le formattage"><button class="jodit-toolbar-button__button" type="button" role="button" aria-pressed="false" tabindex="-1"><span class="jodit-toolbar-button__icon"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792" class="jodit-icon_eraser jodit-icon"> <path d="M832 1408l336-384h-768l-336 384h768zm1013-1077q15 34 9.5 71.5t-30.5 65.5l-896 1024q-38 44-96 44h-768q-38 0-69.5-20.5t-47.5-54.5q-15-34-9.5-71.5t30.5-65.5l896-1024q38-44 96-44h768q38 0 69.5 20.5t47.5 54.5z"></path> </svg></span><span class="jodit-toolbar-button__text"></span></button></span><span class="jodit-toolbar-button jodit-toolbar-button_copyformat jodit-ui-group__copyformat jodit-toolbar-button_size_small" role="listitem" data-ref="copyformat" ref="copyformat" aria-label="Cloner le format"><button class="jodit-toolbar-button__button" type="button" role="button" aria-pressed="false" tabindex="-1"><span class="jodit-toolbar-button__icon"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" class="jodit-icon_copyformat jodit-icon"> <path stroke-width="0" d="M16 9v-6h-3v-1c0-0.55-0.45-1-1-1h-11c-0.55 0-1 0.45-1 1v3c0 0.55 0.45 1 1 1h11c0.55 0 1-0.45 1-1v-1h2v4h-9v2h-0.5c-0.276 0-0.5 0.224-0.5 0.5v5c0 0.276 0.224 0.5 0.5 0.5h2c0.276 0 0.5-0.224 0.5-0.5v-5c0-0.276-0.224-0.5-0.5-0.5h-0.5v-1h9zM12 3h-11v-1h11v1z"></path> </svg></span><span class="jodit-toolbar-button__text"></span></button></span><div class="jodit-ui-separator"></div><span class="jodit-toolbar-button jodit-toolbar-button_symbol jodit-ui-group__symbol jodit-toolbar-button_size_small" role="listitem" data-ref="symbol" ref="symbol" aria-label="Insérer un caractère spécial"><button class="jodit-toolbar-button__button" type="button" role="button" aria-pressed="false" tabindex="-1"><span class="jodit-toolbar-button__icon"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 270 270" class="jodit-icon_omega jodit-icon"> <path d="m240.443652,220.45085l-47.410809,0l0,-10.342138c13.89973,-8.43655 25.752896,-19.844464 34.686646,-33.469923c11.445525,-17.455846 17.496072,-37.709239 17.496072,-58.570077c0,-59.589197 -49.208516,-108.068714 -109.693558,-108.068714s-109.69263,48.479517 -109.69263,108.069628c0,20.860839 6.050547,41.113316 17.497001,58.570077c8.93375,13.625459 20.787845,25.032458 34.686646,33.469008l0,10.342138l-47.412666,0c-10.256959,0 -18.571354,8.191376 -18.571354,18.296574c0,10.105198 8.314395,18.296574 18.571354,18.296574l65.98402,0c10.256959,0 18.571354,-8.191376 18.571354,-18.296574l0,-39.496814c0,-7.073455 -4.137698,-13.51202 -10.626529,-16.537358c-25.24497,-11.772016 -41.557118,-37.145704 -41.557118,-64.643625c0,-39.411735 32.545369,-71.476481 72.549922,-71.476481c40.004553,0 72.550851,32.064746 72.550851,71.476481c0,27.497006 -16.312149,52.87161 -41.557118,64.643625c-6.487902,3.026253 -10.6256,9.464818 -10.6256,16.537358l0,39.496814c0,10.105198 8.314395,18.296574 18.571354,18.296574l65.982163,0c10.256959,0 18.571354,-8.191376 18.571354,-18.296574c0,-10.105198 -8.314395,-18.296574 -18.571354,-18.296574z"></path> </svg></span><span class="jodit-toolbar-button__text"></span></button></span><span class="jodit-toolbar-button jodit-toolbar-button_fullsize jodit-ui-group__fullsize jodit-toolbar-button_size_small" role="listitem" data-ref="fullsize" ref="fullsize" aria-label="Ouvrir l'éditeur en pleine page" aria-pressed="true"><button class="jodit-toolbar-button__button" type="button" role="button" aria-pressed="true" tabindex="-1"><span class="jodit-toolbar-button__icon"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792" class="jodit-icon_shrink jodit-icon"> <path d="M896 960v448q0 26-19 45t-45 19-45-19l-144-144-332 332q-10 10-23 10t-23-10l-114-114q-10-10-10-23t10-23l332-332-144-144q-19-19-19-45t19-45 45-19h448q26 0 45 19t19 45zm755-672q0 13-10 23l-332 332 144 144q19 19 19 45t-19 45-45 19h-448q-26 0-45-19t-19-45v-448q0-26 19-45t45-19 45 19l144 144 332-332q10-10 23-10t23 10l114 114q10 10 10 23z"></path> </svg></span><span class="jodit-toolbar-button__text"></span></button></span></div></div></div></div>
            # Second children 
            <div contenteditable="false" class="jodit-workplace" style="min-height: 44px; height: 620px;"><div contenteditable="true" aria-disabled="false" tabindex="-1" class="jodit-wysiwyg" spellcheck="true" style="min-height: 44px;"></div><span data-ref="placeholder" style="display: block; font-size: 13px; line-height: 21.3333px; text-align: start; margin-top: 0px; margin-left: 0px;" class="jodit-placeholder">Ecrivez ici</span><div class="jodit-search">
                <div class="jodit-search__box">
                    <div class="jodit-search__inputs">
                        <input data-ref="query" tabindex="0" placeholder="Rechercher" type="text">
                        <input data-ref="replace" tabindex="0" placeholder="Remplacer par" type="text">
                    </div>
                    <div class="jodit-search__counts">
                        <span data-ref="counter-box">0/0</span>
                    </div>
                    <div class="jodit-search__buttons">
                        <button data-ref="next" tabindex="0" type="button"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792"> <path d="M1395 736q0 13-10 23l-466 466q-10 10-23 10t-23-10l-466-466q-10-10-10-23t10-23l50-50q10-10 23-10t23 10l393 393 393-393q10-10 23-10t23 10l50 50q10 10 10 23z"></path> </svg></button>
                        <button data-ref="prev" tabindex="0" type="button"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792"> <path d="M1395 1184q0 13-10 23l-50 50q-10 10-23 10t-23-10l-393-393-393 393q-10 10-23 10t-23-10l-50-50q-10-10-10-23t10-23l466-466q10-10 23-10t23 10l466 466q10 10 10 23z"></path> </svg></button>
                        <button data-ref="cancel" tabindex="0" type="button"><svg viewBox="0 0 14 14" xmlns="http://www.w3.org/2000/svg"> <g stroke="none" stroke-width="1"> <path d="M14,1.4 L12.6,0 L7,5.6 L1.4,0 L0,1.4 L5.6,7 L0,12.6 L1.4,14 L7,8.4 L12.6,14 L14,12.6 L8.4,7 L14,1.4 Z"></path> </g> </svg></button>
                        <button data-ref="replace-btn" tabindex="0" type="button" class="jodit-ui-button">Remplacer</button>
                    </div>
			    </div>
		    </div><div class="jodit-source"><div class="jodit-source__mirror-fake ace_editor ace-idle-fingers ace_dark"><textarea class="ace_text-input" wrap="off" autocorrect="off" autocapitalize="off" spellcheck="false" style="opacity: 0; font-size: 1px;"></textarea><div class="ace_gutter" aria-hidden="true"><div class="ace_layer ace_gutter-layer ace_folding-enabled" style="height: 1e+06px;"></div></div><div class="ace_scroller" style="line-height: 0px;"><div class="ace_content"><div class="ace_layer ace_print-margin-layer"><div class="ace_print-margin" style="left: 4px; visibility: visible;"></div></div><div class="ace_layer ace_marker-layer"></div><div class="ace_layer ace_text-layer" style="height: 1e+06px; margin: 0px 4px;"></div><div class="ace_layer ace_marker-layer"></div><div class="ace_layer ace_cursor-layer ace_hidden-cursors"><div class="ace_cursor"></div></div></div></div><div class="ace_scrollbar ace_scrollbar-v" style="display: none; width: 22px;"><div class="ace_scrollbar-inner" style="width: 22px;">&nbsp;</div></div><div class="ace_scrollbar ace_scrollbar-h" style="display: none; height: 22px;"><div class="ace_scrollbar-inner" style="height: 22px;">&nbsp;</div></div><div style="height: auto; width: auto; top: 0px; left: 0px; visibility: hidden; position: absolute; white-space: pre; font: inherit; overflow: hidden;"><div style="height: auto; width: auto; top: 0px; left: 0px; visibility: hidden; position: absolute; white-space: pre; font: inherit; overflow: visible;"></div><div style="height: auto; width: auto; top: 0px; left: 0px; visibility: hidden; position: absolute; white-space: pre; font: inherit; overflow: visible;">XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX</div></div></div></div><div style="position: absolute; color: red; right: 10px; bottom: 10px;"></div></div>
            # Third children
            <div class="jodit-status-bar"><div class="jodit-status-bar__item jodit-status-bar__item-right"><span>Symboles: 0</span></div><div class="jodit-status-bar__item jodit-status-bar__item-right"><span>Mots: 0</span></div><div class="jodit-status-bar__item"><div class="jodit-xpath"><span class="jodit-toolbar-button jodit-toolbar-button_selectall jodit-toolbar-button_size_tiny" role="listitem" data-ref="selectall" ref="selectall" aria-label="Tout sélectionner"><button class="jodit-toolbar-button__button" type="button" role="button" aria-pressed="false" tabindex="-1"><span class="jodit-toolbar-button__icon"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 18" class="jodit-icon_select_all jodit-icon"> <g fill-rule="evenodd" stroke="none" stroke-width="1"> <g transform="translate(-381.000000, -381.000000)"> <g transform="translate(381.000000, 381.000000)"> <path d="M0,2 L2,2 L2,0 C0.9,0 0,0.9 0,2 L0,2 Z M0,10 L2,10 L2,8 L0,8 L0,10 L0,10 Z M4,18 L6,18 L6,16 L4,16 L4,18 L4,18 Z M0,6 L2,6 L2,4 L0,4 L0,6 L0,6 Z M10,0 L8,0 L8,2 L10,2 L10,0 L10,0 Z M16,0 L16,2 L18,2 C18,0.9 17.1,0 16,0 L16,0 Z M2,18 L2,16 L0,16 C0,17.1 0.9,18 2,18 L2,18 Z M0,14 L2,14 L2,12 L0,12 L0,14 L0,14 Z M6,0 L4,0 L4,2 L6,2 L6,0 L6,0 Z M8,18 L10,18 L10,16 L8,16 L8,18 L8,18 Z M16,10 L18,10 L18,8 L16,8 L16,10 L16,10 Z M16,18 C17.1,18 18,17.1 18,16 L16,16 L16,18 L16,18 Z M16,6 L18,6 L18,4 L16,4 L16,6 L16,6 Z M16,14 L18,14 L18,12 L16,12 L16,14 L16,14 Z M12,18 L14,18 L14,16 L12,16 L12,18 L12,18 Z M12,2 L14,2 L14,0 L12,0 L12,2 L12,2 Z M4,14 L14,14 L14,4 L4,4 L4,14 L4,14 Z M6,6 L12,6 L12,12 L6,12 L6,6 L6,6 Z"></path> </g> </g> </g> </svg></span><span class="jodit-toolbar-button__text"></span></button></span>﻿</div></div></div>
        */
        let joditNode = nodeList.item(0)
        if(joditNode.children[1] != null){
            //We need the second children to manipulate the DOM property
            this.joditNodeChildren = joditNode.children[1].children[0];
            this.joditNodeChildren.contentEditable = true;
  
            // read $dataname value and display in control
            var dataname = this.getData();
            if (this.lastData != dataname) { // if $dataname value has changed then reset it
                this.lastData = dataname;
            }else{
                this.lastData = this.joditNodeChildren.innerHTML;
            }
           
            if (this.mData != this.lastData || this.lastData == "") { // only execute the following code if the value of the $dataname variable has changed.
                this.setData(this.lastData, null, null, null, "s");
                if(eventJodit == null){ // if none event is triggered then we reset HTML content
                    this.joditNodeChildren.innerHTML = this.lastData;
                }
            }
        }
        return ctrl;
    };

    /**
     * This is called when an event registered using this.mEventFunction() is triggered.
     *
     * @param event The event object
     */
    ctrl.handleEvent = function( event ) {
        if (!this.isEnabled()) return true; // If the control is disabled, don't process the event.
        switch (event.type) {
            case "click":
                if (this.canSendEvent(eBaseEvent.evClick))  {
                    this.eventParamsAdd("pPosX", event.offsetX);
                    this.eventParamsAdd("pPosY", event.offsetY);
                    this.sendEvent(eBaseEvent.evClick);
                    return;
                }
            case "omnis-setdata":
                if(this.joditNodeChildren != null){ 
                    var data = this.joditNodeChildren.innerHTML;
                    if (this.lastData == null || data != this.lastData) {
                        this.lastData = data;
                        this.setData(data, null, null, null, "s");
                        // We do not support after events as we cannot do this reliably.
                        // this.setPlain(data);
                    }
                    return true;
                }
            case "typing":{   
                if(afterTimeout != undefined) clearTimeout(afterTimeout)
                    if (this.getProperty(PROPERTIES.enablekeypressevent)){
                        afterTimeout = setTimeout(()=>{
                            this.sendEvent(eBaseEvent.evAfter)
                        },this.getProperty(PROPERTIES.callbackdelay))
                        return true;
                    }
                }
        }
        return this.superclass.handleEvent.call( this, event ); //Let the superclass handle the event, if not handled here.
    };

    /**
     * Called to get the value of an Omnis property
     *
     * @param propNumber    The Omnis property number
     * @returns {var}       The property's value
     */
    ctrl.getProperty = function(propNumber)
    {
        switch (propNumber) {
            case eBaseProperties.text:
                return this.mText;
            // <OmnisUpdateMarker_GetProperty_Start>
            case PROPERTIES.autoscroll:
                return this.mautoscroll;
            case PROPERTIES.enablekeypressevent:
                return this.menablekeypressevent;
            case PROPERTIES.callbackdelay:
                return this.mcallbackdelay;
            // <OmnisUpdateMarker_GetProperty_Next>
        }
        return this.superclass.getProperty.call(this, propNumber); //Let the superclass handle it,if not handled here.
    };

    /**
     * Function to get $canassign for a property of an object
     * @param propNumber    The Omnis property number
     * @returns {boolean}   Whether the passed property can be assigned to.
     */
    ctrl.getCanAssign = function(propNumber)
    {
        switch (propNumber) {
            case eBaseProperties.text:
                return true;
            // <OmnisUpdateMarker_GetCanAssign_Start>
            case PROPERTIES.autoscroll:
                return true;
            case PROPERTIES.enablekeypressevent:
                return true;
            case PROPERTIES.callbackdelay:
                return true;
            // <OmnisUpdateMarker_GetCanAssign_Next>
        }
        return this.superclass.getCanAssign.call(this, propNumber); // Let the superclass handle it,if not handled here.
    };

    /**
     * Assigns the specified property's value to the control.
     * @param propNumber    The Omnis property number
     * @param propValue     The new value for the property
     * @returns {boolean}   success
     */
    ctrl.setProperty = function( propNumber, propValue )
    {
        if (!this.getCanAssign(propNumber)) // check whether the value can be assigned to
            return false;

        switch (propNumber) {
            case eBaseProperties.text: // Set the text as appropriate for this control.
                this.mText = propValue;
                var client_elem = this.getClientElem();
                client_elem.innerHTML = propValue;
                return true;
            // <OmnisUpdateMarker_SetProperty_Start>
            case PROPERTIES.autoscroll:
                this.mautoscroll = propValue;
                return true;
            case PROPERTIES.enablekeypressevent:
                this.menablekeypressevent = propValue;
                return true;
            case PROPERTIES.callbackdelay:
                this.mcallbackdelay = propValue;
                return true;
            // <OmnisUpdateMarker_SetProperty_Next>
        }
        return this.superclass.setProperty.call( this, propNumber, propValue ); // Let the superclass handle it, if not handled here.
    };

    /**
     * Called when the size of the control has changed.
     */
    ctrl.sizeChanged = function ()
    {
        this.superclass.sizeChanged();

        // center any text vertically
        var elem = this.getClientElem();
        elem.style.lineHeight = elem.style.height
    };

    /**
     * Called when visibility of the control has changed.
     * eg. when used on a paged pane and the current page has changed.
     */
    ctrl.visibilityChanged = function ()
    {
        this.superclass.visibilityChanged.call(this);
        if (this.getTrueVisibility(null))
        {
            this.update();
            // your code where required, could be this.update(), this.sizeChanged() or other
        }
    };

    // <OmnisUpdateMarker_CustomMethod_New>

    return ctrl;
})();

/**
 * Constructor for our control.
 * @constructor
 */
function ctrl_net_omnis_jodit()
{
    this.init_class_inst(); // initialize our class
}
