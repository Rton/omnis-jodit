List of changes :
I. 	Control script file (ctrl_net_omnis_jodit.js) : 	
1.	Each new lines begin with `<br/>` instead of `<p>`
2.	Omnis Studio send 'evTyping' type event when user is typing with evAfter as callback.
3.	Each callback is triggered after X ms as X is value of `callbackdelay` property and if `enablekeypressvent` set to kTrue (you can locate it on `Jodit Properties` tab)
4.	Callbacks are only enable if user stops writing after X ms 


II.	Omnis object control file (control.json):
	Remove unecessary field and append required control