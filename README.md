# omnis-jodit

Omnis Studio 10.1 with Jodit Open Source WYSIWYG editor written in pure TypeScript without using additional libraries.

Omnis Studio : https://www.omnis.net/
Jodit v.3 : https://xdsoft.net/jodit/

## Requirements
- [ ] Omnis Studio 10.1
- [ ] JQuery


## Visuals
![127.0.0.1_25000_jschtml_jsPrincipal.htm](/uploads/ce93e353cc07794a719a2946411e0355/127.0.0.1_25000_jschtml_jsPrincipal.htm.png)

## Installation
1. Close Omnis Studio first.
2. Place the net.omnis.jodit folder in \ html \ controls of your Omnis Studio.
3. Copy the files from the css folder to the \ html \ css folder of your Omnis Studio.
4. Copy the files from the scripts folder to the \ html \ scripts folder of your Omnis Studio.
5. Add the following lines to your \ html \ jsctempl.htm file

Below meta tag
````
<script type="text/javascript" src="scripts/jquery.min.js"> </script>
````
Below CSS line
````
<link type="text/css" href="css/jodit.css" rel="stylesheet"/>
````
Below JavaScript line
````
<script type="text/javascript" src="scripts/jodit.js"> </script>
<script type="text/javascript" src="scripts/config.js"> </script>
<script type="text/javascript" src="scripts/ctl_net_omnis_jodit.js"> </script>
````
6. Start Omnis Studio
7. Open the library 

## Usage
Specialized for word processing for most of documents

## Collaboration
I'm open for collaboration, feel free to contact me.

## Support
You can contact me from my Github account : R-ton, or my GitLab account : Rton, or give an issue directly from this repository.

## Roadmap
- [ ] Test and bug reporting
- [ ] Correction
- [ ] Development
- [ ] [ ] Configure toolbar option
- [ ] Uprade to Omnis Studio 10.2

## Contributing
You can contribute by reporting bugs, making suggestion for enhancements on the issues panel, or pull requests.

## Authors and acknowledgment
I feel grateful for those who contributes in this project.
Please feel free to make suggestions.
## License
Under MIT License.

## Project status
This version is ready to be used but some bug features may happen.

